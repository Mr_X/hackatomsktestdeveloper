let x = 0;
let f = () => {
    console.log(x * 2);
    x++;
    if (x < 10)
        f.timeout = setTimeout(f, Math.random() * 1000);
};
f();